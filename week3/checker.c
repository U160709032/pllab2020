#include <stdio.h>

int isPrime(int number, int half)
{
    if (half == 1)
    {
        return 1;
    }
    else
    {
       if (number % half == 0)
       {
         return 0;
       }
       else
       {
         return isPrime(number, half - 1);
       }       
    }
}
  
int main()
{
    int number, checker;
    printf("Enter a number: ");
    scanf("%d", &number);
    checker = isPrime(number, number / 2);
    if(number ==1)
    {
    	printf("Not Prime");
    }
    else
    {
    	if (checker == 1)
        	printf("Prime\n");
    	else
        	printf("Not Prime\n");
    }
    return 0;
}

