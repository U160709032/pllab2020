#include <stdio.h>
 
int intnrDigits(int num)
{
    static int count=0;
     
    if(num>0)
    {
        count++;
        intnrDigits(num/10);
    }
    else
    {
        return count;
    }
}
int main()
{
    int number;
    int count=0;
     
    printf("Enter number: ");
    scanf("%d",&number);
     
    count=intnrDigits(number);     
    printf("Number of digits: %d\n",count);
     
    return 0;
}
